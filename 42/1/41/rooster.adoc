== Rooster 21-22

|===
|Dag|Tijd|Klascode|EO|Vak
|Maandag|08:30-09:00|Weekstart||
|	|09:00-10:00|0ITa4TIC|3-2|SLB
|	|10:00-11:00|0ITe4TIC|4-2|DC
|	|11:30-12:30|1ITe4TIC|4-1|DC
|Dinsdag|09:00-10:00|1ITa4TIC|3-1|Linux
|	|10:00-11:00|0ITe4TIC|4-2|DC
|	|11:30-12:30|1ITb4TIC|3-1|DC
|	|13:00-15:00|1ITa4TIC|3-1|Project
|Woensdag|09:00-10:00|0ITe4TIC|4-2|DC
|	|11:30-12:30|7.8.9NB|4-3|DC
|	|13:00-15:00|01Ta4TIC|3-2|Project
|Donderdag|13:00-14:30|7.8.9NB|4-3|DC
|Vrijdag|10:00-11:00|1ITe4TIC|4-1|DC
|	|11:30-12:30|1ITb4TIC|3-1|DC
|===




