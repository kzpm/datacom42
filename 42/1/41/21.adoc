= Module 2.1 Toegang tot het Cisco IOS
:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud
:last-update-label: H.Werker, C.W.Epema, R.Baron

link:http://localhost/kbase.nl/datacom/41/index.html[Terug]

== 2.1 Toegang tot Cisco IOS

Besturingssysteem:

- CLI (Command Line Interface)
- GUI (Graphical User Interface)

|===
|Shell| Gebruikersinterface waarmee gebruikers taken kunnen laten uitvoeren. Kan via CLI of GUI
|Kernel|Communicatie tussen hardware en software. Ook bedoeld om bij te houden welke bronnen beschikbaar zijn
|Hardware| Fysieke deel van de computer incl. electronica
|===

Bij de CLI interactie direct taken via ingtoetste opdrachten uitvoeren. Geeft weinig overhead en is dus over het algemeen resource friendly in vergelijking met de GUI.

=== 2.1.2 GUI

Bijna iedereen werkt ondanks de voordelen van een CLI met een GUI. Windows en MacOs werken op deze manier. Met behulp van pictogrammen voert de gebruiker taken uit. De gebuiker wordt minder geconfronteerd met commando's en wordt daardoor in het algemeen als gebruiksvriendelijk ervaren.

Windows GUI:

image::https://images.gutefrage.net/media/fragen/bilder/windows-10-desktop-ganz-verschwunden-hilfe/0_original.jpg[title="Windows 10 desktop"]

MacOs GUI:

image::https://cdn.mos.cms.futurecdn.net/MQ7pS97SmQd466BoeTPrkj-320-80.jpg[title="MacOs desktop"]

Linux GUI:

image::https://cdn.mos.cms.futurecdn.net/Djrhv5WTrhyqvUAXN6hFhi-320-80.jpg[title="Linux GUI (Gnome)"]


Linux CLI:

image::cli.png[title="CLI Linux"]


==== 2.1.2.1 IOS en Firmware

Op Cisco apparatuur draait het operating system IOS (Internetwork Operating System).
Het lijkt een beetje op de CLI van Linux, maar sommige opdrachten komen ook weer overeen met Windows CLI.

Je thuisrouter is waarschijnlijk geen Cisco product. De meeste routers en switches, die niet van Cisco zijn, kunnen ook met een webinterface (via de browser dus) benaderd worden.
In de regel wordt het besturingssysteem van zo'n router of switch ook wel eens *firmware* genoemd.

Per merk ziet de webinterface er weer verschillend uit. Hieronder een voorbeeld van een Linksys router webinterface:

image::https://i1.wp.com/www.alluthink.com/wp-content/uploads/2016/11/linksys-simulators-screenshot2.png[title="Webinterface Linksys"]

=== 2.1.3 Doel van een OS

Het besturingssysteem heeft als doel om een apparaat taken te laten uitvoeren. Dit kunnen taken zijn die door een gebruiker worden gestart, maar ook services, die automatisch op de achtergrond draaien.

Cisco gebruikt IOS. De bediening van het besturingssysteem vindt voor de gebruiker plaats via een CLI.

=== 2.1.4 Toegangsmethoden tot intermediaire apparaten (Routers, switches)

*Routers en switches*
|===
|Console| Out-of-band toegang. Toegang via een speciale kabel. In PT wordt deze kabel als lichtblauw weergegeven. Kan alleen plaatsvinden op het fysieke apparaat.
|SSH (Secure shell)|In-band toegang. Aanbevolen methode. Heel veilig. Standaard versleuteld dataverkeer via PKI (Public Key Infrastructure). Vereist wel dat op het te managen apparaat een ssh service draait.
|Telnet|In-band toegang. Onveilige (onversleutelde) dataverbinding. Mag alleen in LAB omgevingen worden gebruikt. Beslist NIET voor productieomgevingen!
|===

image:https://images-na.ssl-images-amazon.com/images/I/61PSzg3KbzL._AC_SL1000_.jpg[title="Consolekabel",400x300]

In de regel is er voor switches wat minder configuratiewerk nodig. 
Standaard staan alle switchports immers open op Cisco switches. Dit betekent dat eindapparaten, die aangesloten worden op de switch onmiddelijk met elkaar kunnen communiceren.

In de theorie staat *'out-of-band management'* als toegangstype. Hiermee wordt bedoeld: toegang tot een device buiten de reguliere netwerktoegang van eindapparaten. Dit is nofig, wanneer er geen gebruik meer kan gemaakt worden van het bedrijfsnetwerk.
Daar tegen over staat het begrip *in-band management*. Hierbij worden apparaten via het bestaande bedrijfsnetwerk benaderd.

Op oudere apparaten vind je vaak ook nog een AUX (Auxiliary) toegangspoort. Deze poort kan gebruikt worden als out-of-band toegang over een modem en telefoonlijn.

image::http://1.bp.blogspot.com/-2M4_HrpQam4/Tu-wLzk7LGI/AAAAAAAAAco/V3SNw5ewlmw/w1200-h630-p-k-no-nu/AUXILIARY+PORT+GNS3.png[title="AUX port",400x400]

=== 2.1.5 Terminal emulatie programma's

Een terminal is hetzelfde als een CLI omgeving. In Linux en MacOs is standaard een ssh client/server ingebouwd. In Windows niet.
Om in Windows toegang te krijgen tot een router of switch heb je een terminal-emulator nodig.
Er zijn een aantal bekende terminal-emulatie oplossingen voor het Windows platform. 


https://www.chiark.greenend.org.uk/~sgtatham/putty/[Putty] 

image::http://wpsites.net/wp-content/uploads/2012/02/Putty-SSH-Client.png[title="Putty"]

https://www.vandyke.com/products/securecrt/[SecureCRT]

image::https://cdn.lo4d.com/t/screenshot/800/securecrt-3.jpg[title="SecureCRT"]

https://ttssh2.osdn.jp/index.html.en[Teraterm]

image::http://write-remember.com/wordpress/wp-content/uploads/2016/01/a1ca178065be4d43a856fc126d777faa.png[title="Teraterm"]

