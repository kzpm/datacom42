= Module 1.8 Netwerk security
:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud

link:http://localhost/kbase.nl/datacom/41/index.html[Terug]

== 1.8 Netwerk security


=== 1.8.1 Security threats 

Meest voorkomende bedreigingen:

|===
|Virussen, trojans, wormen|Bevatten schadelijke software, die op een target uitgevoerd kan worden
|Spy- en adware|Wanneer ze op een end-device geinstalleerd zijn, verzamelt deze software in het geheim informatie
|Zero-day attacks|Maakt gebruik van zwakke plekken in software. Zo genoemd omdat de kwetsbaarheid uitgebuit wordt, voordat de ontwikkelaars van de kwetsbare software erachter zijn gekomen. (Onlangs bijv. nog een jaren lang probleem in Microsoft Exchange en in de MS Printspool service)
|Hacker-attacks|Een kwaadwillende persoon of organisatie, valt netwerk-resources of eindgebruikersapparaten aan
|Denial of Service (DoS)| Het netwerk van het slachtoffer wordt zodanig belast, dat applicaties en systemen uiteindelijk niet meer reageren.
|Man in the middle (MITM)| Deze aanvallen worden toegepast om gevoelige informatie vanaf een bedrijfsnetwerk te onderscheppen.
|Identiteitsdiefstal|De aanvaller steelt login gegevens van een gebruiker om toegang tot gevoelige informatie te krijgen.
|===


=== 1.8.2 Security oplossingen

Er is geen directe oplossing om netwerken te beschermen. De beveiliginging zal op veel lagen plaats moeten vinden.

Voor een thuisnetwerk volstaat het meestal om de eindapparaten te beveiligen. Je kunt ervan uitgaan dat de ISP de beveiliging op orde heeft voor zover mogelijk.

Een aantal maatregelen, die tegenwoordig vaak toegepast worden:

|===
|Antivirus en anti-spyware| Voorkomen dat eindapparaten besmet kunnen worden
|Firewall filtering| Voorkomt ongeauthoriseerd toegang. Kan geimplementeerd worden op eindapparaten en intermediaire apparaten (routers, switches)
|Dedicated-firewall systemen|Meer voor grote bedrijven en organisaties. Hiermee kan dataverkeer zeer fijnmazig gefilterd worden
|Access-control lists (ACL)|Deze filteren toegang en doorsturen van data op basis van adressering, poortnummering en protocols/applicaties
|Intrusion Prevention System (IPS)|Bedoeld om snel verspreidende software (zero day attacks) te identificeren
|Virtual Private Network| Beveiligde toegang voor medewerkers naar een bedrijfsnetwerk
|===


image::https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/OpenVPN_logo.svg/1280px-OpenVPN_logo.svg.png[OpenVPN]
