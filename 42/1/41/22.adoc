= Module 2.2 IOS Navigatie
:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud
:last-update-label: H.Werker, C.W.Epema, R.Baron

link:http://localhost/kbase.nl/datacom/41/index.html[Terug]

2.2 IOS Navigatie

== 2.2.1  Belangrijke commando modes in IOS

.Commando modes
|===
|Commando-mode|Beschrijving|Prompt
|User-Exec mode|Beperkte toegang mogelijk. Niet mogelijk om configuratie aan te pasen van switch of router.|Switch> Router>
|Privileged-Exec mode|Vanuit deze mode, is volledige configuratie mogelijk.|Switch# Router#
|Global-configuration mode|In deze mode ben je bezig met het configureren van globale instellingen van je router of switch| Switch(config)# Router(config)#
|Line-configuratie mode| Wordt gebruikt voor configuratie van SSH, Telnet of AUX toegang.|Switch(config-line)# Router(config-line)#
|Interface-configuratie modus| Wordt gebruikt om een netwerkinterface of een switch poort te configureren| Switch(config-int)# Router(config-int)#
|===


=== 2.2.3

