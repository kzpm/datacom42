:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud
:42: https://kbase.nl/datacom/42/1/
:soc: https://b.socrative.com/login/student/

== {42}[N42 CCNA2 Switching en Routing Essentials (SWRE)]

=== 8. SLAAC en DHCPv6


==== 8.0.1 Welkom bij SLAAC en DHCPv6!

SLAAC en DHCPv6 zijn dynamische adresseringsprotocollen voor een IPv6 netwerk. Een beetje configuratie zal je dag als netwerkbeheerder dus een stuk gemakkelijker maken. In deze module leert u hoe u SLAAC gebruikt om hosts in staat te stellen hun eigen IPv6 global unicast-adres aan te maken, en hoe u een Cisco IOS-router configureert als een DHCPv6-server, een DHCPv6-client of een DHCPv6-relayagent. Deze module bevat een practicum waarin u DHCPv6 op echte apparatuur configureert!

==== 8.0.2 Wat ga ik leren?

Module titell: SLAAC en DHCPv6
Doel: Stel automatische adressering in op IPv6 netwerken.

[%header,cols=2]
|===
|Onderwerp|Doel
|IPv6 global Unicast Address Assignement|J ekunt uitleggen hoe een host aan de nodige IPv6 informatie kan komen
|SLAAC|Je kunt uitleggen hoe SLAAC werkt
|DHCPv6|Je kunt uitleggen hoe DHCPv6 werkt
|Configureer een DHCPv6 server|Configureer een *statefull* en *stateless* DHCPv6 server
|===

=== 8.1 IPv6 Global Unicast Address (GUA) toewijzing


==== 8.1.1 Host configuratie

Om ofwel stateless address autoconfiguration (SLAAC) of DHCPv6 te gebruiken, moet je _eerst_ globale unicast adressen (GUAs) en link-lokale adressen (LLAs) kennen. 
Dit onderwerp behandelt beide manieren van adressering.

Op een router wordt een IPv6 global unicast-adres (GUA) handmatig geconfigureerd met de opdracht `ipv6-adres <ipv6-adres/prefix-lengte>` in de  interfaceconfiguratie modus.

Controleer als root in een Linux terminal of ipv6 adressering is ingeschakeld:

 # sysctl -a | grep ipv6.*disable
 net.ipv6.conf.all.disable_ipv6 = 0
 net.ipv6.conf.default.disable_ipv6 = 0
 net.ipv6.conf.eno49.disable_ipv6 = 0

De _0_ betekent dat ipv6 is ingeschakeld. Wanneer deze bij jou op _1_ staat, is ipv6 uitgeschakeld.

Via de netwerkmanager of met cli kun je ipv6 inschakelen. Gebruik je favoriete zoekmachine om uit te vinden hoe je ipv6 aan kunt zetten.

Omdat er makkelijk fouten gemaakt kunnen worden wanneer ipv6 adressen handmatig worden ingevoerd, is het raadzaam om dit over te laten aan een ipv6 dhcp service.


==== 8.1.2 IPv6 Host Link Local Address (LLA)

Wanneer automatische IPv6-adressering geselecteerd is, zal de host proberen om automatisch IPv6-adresinformatie op de interface te halen en te configureren. 
De host zal een van de drie methoden gebruiken, die worden gedefinieerd door het Internet Control Message Protocol versie 6 (ICMPv6) Router Advertisement (RA) bericht dat op de interface wordt ontvangen. 
Een IPv6-router die zich op dezelfde verbinding als de host bevindt, zendt RA-berichten uit die de hosts voorstellen hoe zij hun IPv6-adresinformatie kunnen verkrijgen. 
Het IPv6-link-lokaal adres wordt automatisch door de host aangemaakt wanneer deze opstart en de Ethernet-interface actief is. 
De uitvoer van het voorbeeld ipconfig toont een automatisch gegenereerd link-local adres (LLA) op een interface.

image::{42}8.1.2a.png[]

In de figuur is te zien dat de interface ook een IPv6 GUA heeft. De reden hiervoor is dat, in dit voorbeeld, het netwerksegment een router heeft om netwerkconfiguratie-instructies voor de host te verzorgen of dat de host niet met een statisch IPv6-adres is geconfigureerd.

NOTE: Hostbesturingssystemen tonen soms een link-lokaal adres met een "%" en een nummer. Dit staat bekend als een Zone ID of Scope ID. Het wordt door het besturingssysteem gebruikt om de LLA aan een specifieke interface te koppelen.

==== 8.1.3 IPv6 GUA toewijzing

IPv6 is ontworpen om de manier waarop een host zijn IPv6-configuratie kan verkrijgen te vereenvoudigen. 
Standaard maakt een IPv6-geschikte router zijn IPv6-informatie bekend. 
Hierdoor kan een host zijn IPv6-configuratie dynamisch aanmaken of ophalen.

De IPv6 GUA kan dynamisch worden toegewezen met behulp van *stateless* en *stateful* services, zoals in de figuur wordt getoond.

Alle stateless en stateful methoden in deze module gebruiken ICMPv6 RA-berichten om de host voor te stellen hoe hij zijn IPv6-configuratie moet aanmaken of verkrijgen. 
Hoewel hostbesturingssystemen de suggestie van de RA volgen, is de werkelijke beslissing uiteindelijk aan de host.

image::{42}8.1.3a.png[]

==== 8.1.4 Drie RA berichtvlaggen

De beslissing over hoe een client een IPv6 GUA zal verkrijgen hangt af van de *instellingen in het RA bericht*.

Een ICMPv6 RA bericht bevat *drie vlaggen* om de dynamische opties te identificeren die beschikbaar zijn voor een host, als volgt:

- A flag - Dit is de Address Autoconfiguration flag. Gebruik Stateless Address Autoconfiguration (SLAAC) om een IPv6 GUA te maken.
- O flag - Dit is de vlag Other Configuration (Andere configuratie). Andere informatie is beschikbaar van een stateless DHCPv6 server.
- M vlag - Dit is de Managed Address Configuration vlag. Gebruik een stateful DHCPv6 server om een IPv6 GUA te verkrijgen.

Door verschillende combinaties van de A, O en M vlaggen te gebruiken, informeren RA berichten de host over de beschikbare dynamische opties.

De figuur illustreert deze drie methodes.

image::{42}8.1.4a.png[]


==== 8.2.1 Overzicht SLAAC 

Niet elk netwerk heeft toegang tot een DHCPv6 server of heeft dat nodig. 
Maar elk apparaat in een IPv6 netwerk heeft een GUA nodig. 
De SLAAC methode stelt hosts in staat om hun eigen unieke IPv6 globaal unicast adres aan te maken zonder de diensten van een DHCPv6 server.

SLAAC is een stateless service. Dit betekent dat er geen server is die netwerkadresinformatie bijhoudt om te weten welke IPv6-adressen worden gebruikt en welke beschikbaar zijn.

SLAAC gebruikt ICMPv6 RA-berichten om adresserings- en andere configuratie-informatie te verstrekken, die normaal door een DHCP-server zou worden verstrekt. 
Een host configureert zijn IPv6-adres op basis van de informatie die in de RA wordt verzonden. 
RA-berichten worden elke 200 seconden door een IPv6-router verzonden.

Een host kan ook een Router Solicitation (RS)-bericht verzenden waarin wordt verzocht dat een IPv6-geschikte router de host een RA stuurt.

SLAAC kan worden geïmplementeerd als alleen SLAAC, of SLAAC met DHCPv6.

==== 8.2.2 SLAAC Activeren

Raadpleeg de volgende topologie. Hier kun je zien hoe SLAAC geactiveerd wordt om stateless dynamische GUA toewijzing te bieden.

image::{42}8.2.2a.png[]


Stel dat R1 GigabitEthernet 0/0/1 is geconfigureerd met de aangegeven IPv6 GUA en link-local adressen. 

Uitleg over hoe R1 is ingeschakeld voor SLAAC:

*Controleer eerst de huidige ipv6 configuratie:*

De uitvoer van het commando `show ipv6 interface` toont de huidige instellingen op de interface G0/0/1.

R1 heeft de volgende IPv6-adressen toegewezen gekregen (schuin gedrukt):

Link-local IPv6 address - _fe80::1_ +
GUA and subnet - _2001:db8:acad:1::1 and 2001:db8:acad:1::/64_ +
IPv6 all-nodes group - _ff02::1_

image::{42}8.2.2b.png[]


*Activeer IPv6 routing*

Hoewel de routerinterface een IPv6-configuratie heeft, is deze nog niet ingeschakeld voor het verzenden van RA's met adresconfiguratiegegevens naar hosts die SLAAC gebruiken.

Om het verzenden van RA-berichten in te schakelen, moet een router lid worden van de IPv6 all-routers groep met het `ipv6 unicast-routing` global config commando, zoals in de uitvoer te zien is.

image::{42}8.2.2c.png[]

*Controleer of SLAAC geactiveerd is*

De IPv6 all-routers group antwoordt op het IPv6 multicast adres ff02::2. 
Je kunt het commando `show ipv6 interface` gebruiken om te controleren of een router is ingeschakeld, zoals in de uitvoer wordt getoond.

Een IPv6-geschakelde Cisco-router verzendt elke 200 seconden RA-berichten naar het IPv6 all-routers multicast-adres ff02::1.

image::{42}8.2.2d.png[]


==== 8.2.3 De SLAAC-only methode

De SLAAC only methode wordt standaard ingeschakeld wanneer het `ipv6 unicast-routing` commando wordt geconfigureerd. 
Alle ingeschakelde Ethernet interfaces met een geconfigureerde IPv6 GUA zullen beginnen met het verzenden van RA berichten met de A vlag op 1, en de O en M vlaggen op 0, zoals getoond in de figuur.

image::{42}8.2.3a.png[]

De A = 1 vlag vertelt de client, dat hij zijn eigen IPv6 GUA moet creëren met de prefix die in de RA geadverteerd wordt. 
De cliënt kan zijn eigen Interface ID aanmaken met behulp van de Extended Unique Identifier methode (EUI-64) of deze willekeurig laten genereren.

De O =0 en M=0 vlaggen instrueren de client om uitsluitend de informatie in het RA bericht te gebruiken. 
Het RA bevat de prefix, prefix-lengte, DNS server, MTU, en standaard gateway informatie. 
Er is geen verdere informatie beschikbaar van een DHCPv6 


In het voorbeeld is PC1 ingeschakeld om zijn IPv6-adresseringsinformatie automatisch te krijgen. 
Vanwege de instellingen van de A, O en M vlaggen, voert PC1 alleen SLAAC uit, gebruikmakend van de informatie in het RA-bericht dat door R1 wordt verzonden.

Het standaard gateway-adres is het IPv6-bronadres van het RA-bericht, dat de LLA voor R1 is. 
De standaard gateway kan _alleen_ automatisch uit het RA-bericht worden verkregen. 
Een DHCPv6-server verschaft deze informatie niet.

image::{42}8.2.3b.png[]

==== 8.2.4 ICMPv6 RS berichten (Router Sollicitation)

Een router verstuurt elke 200 seconden RA-berichten. Hij zendt echter ook een RA-bericht als hij een RS-bericht van een host ontvangt.

Wanneer een client is geconfigureerd om zijn adresseringsinformatie automatisch te verkrijgen, stuurt hij een RS-bericht naar het IPv6 all-routers multicast-adres ff02::2.

De afbeelding illustreert hoe een host de SLAAC-methode start.


image::{42}8.2.4a.png[]

 1. PC1 is net opgestart en heeft nog geen RA bericht ontvangen. Daarom verstuurt PC1 een RS bericht naar alle IPv6 routers met het multicast adres *ff02::2* in het netwerk, waarin het vraagt om een RA bericht.
 2. R1 is lid van de IPv6 -all-routers groep en ontvangt het bericht. R1 maakt een RA aan met de volgende info: de *local netwerk prefix* en de lengte van de prefix (bijvoorbeeld: 2001:db8:acad:1::/64). 
 Daarna wordt het RA bericht naar het IPv6-all-nodes multicast adres gestuurd *ff02::1*. 
 Met deze informatie is PC1 in staat een uniek IPv6 adres GUA adres aan te maken.

==== 8.2.5 Proces op de host om een interface ID te genereren

Met SLAAC krijgt een host normaal gesproken, zijn 64-bit IPv6-subnetinformatie van de router RA. 
Hij moet echter de resterende 64-bits interface-identifier (ID) genereren volgens een van de volgende twee methoden:

1. Willekeurig gegenereerd - De 64-bit interface ID wordt willekeurig gegenereerd door het client-besturingssysteem. Dit is de methode die nu door Windows 10 hosts wordt gebruikt.
2. EUI-64 - De host maakt een interface-ID met behulp van zijn 48-bits MAC-adres. De host voegt de hex-waarde fffe in het midden van het adres in, en draait de zevende bit van de interface-ID om. 
Dit verandert de waarde van het tweede hexadecimale cijfer van de interface ID. 
Sommige besturingssystemen gebruiken standaard de willekeurig gegenereerde interface-ID in plaats van de EUI-64 methode, vanwege privacy-overwegingen. 
Dit komt omdat het Ethernet MAC-adres van de host door EUI-64 gebruikt wordt om de interface-ID te maken.

image::{42}eui-64.1.png[]

image::{42}eui-64.2.png[]

NOTE: Windows, Linux en Mac OS staan toe om het genereren van de interface-ID te wijzigen. De gebruiker kan kiezen of de ID willekeurig gegenereerd wordt of dat EUI-64 gebruikt wordt.

Bijvoorbeeld, in de volgende ipconfig uitvoer gebruikt de Windows 10 PC1 host de IPv6 subnetinformatie in de R1 RA en genereert hij willekeurig een 64-bit interface ID zoals aangegeven in de uitvoer.

image::{42}8.2.5a.png[]


==== 8.2.6 Duplicate Address Detection

SLAAC stelt de host in staat een IPv6-adres aan te maken. Er is echter geen garantie dat het adres uniek is op het netwerk.

SLAAC is een stateless proces; daarom heeft een host de optie om te verifiëren dat een nieuw aangemaakt IPv6-adres uniek is voordat het kan worden gebruikt. 
Het Duplicate Address Detection (DAD) proces wordt door een host gebruikt om te verzekeren dat het IPv6 GUA uniek is.

DAD wordt geïmplementeerd met behulp van ICMPv6. 
Om DAD uit te voeren stuurt de host een ICMPv6 Neighbor Solicitation (NS) bericht met een speciaal geconstrueerd multicast adres, dat een solicited-node multicast adres wordt genoemd. 
Dit adres dupliceert de laatste 24 bits van het IPv6 adres van de host.

Als geen andere apparaten antwoorden met een NA-bericht, dan is het adres vrijwel gegarandeerd uniek en kan het gebruikt worden door de host. 
Als de host wel een NA ontvangt, is het adres niet uniek en moet het besturingssysteem een nieuwe interface-ID bepalen om te gebruiken.

De Internet Engineering Task Force (IETF) beveelt aan dat DAD wordt gebruikt op alle IPv6 unicast-adressen, ongeacht of deze zijn aangemaakt met alleen SLAAC, zijn verkregen met stateful DHCPv6, of handmatig zijn geconfigureerd. 
DAD is niet verplicht omdat een 64-bit interface ID 18 quintiljoen mogelijkheden biedt en de kans dat er een duplicatie is klein is. 
De meeste besturingssystemen voeren echter DAD uit op alle IPv6 unicast adressen, ongeacht hoe het adres is geconfigureerd.


==== 8.2.7 SLAAC test je kennis (Uitvoeren op netacad)


=== 8.3 DHCPv6

==== 8.3.1 DHCPv6 werking

Dit onderwerp behandelt *stateless* en *stateful DHCPv6*. 
Stateless DHCPv6 gebruikt delen van SLAAC om ervoor te zorgen dat alle noodzakelijke informatie aan de host wordt geleverd. 
Stateful DHCPv6 vereist geen SLAAC.

Hoewel DHCPv6 vergelijkbaar is met DHCPv4, zijn de twee protocollen onafhankelijk van elkaar.

NOTE: DHCPv6 is gedefinieerd in *RFC 3315*.

De host begint de DHCPv6-client/server-communicatie nadat *stateless* DHCPv6 of *stateful* DHCPv6 is aangegeven in de *RA*.

Server-naar-client DHCPv6-berichten gebruiken UDP-bestemmingspoort *546*, terwijl client-naar-server DHCPv6-berichten UDP-bestemmingspoort *547* gebruiken.

De stappen voor DHCPv6-operaties zijn als volgt:

1. De host zendt een RS-bericht.
2. De router antwoordt met een RA-bericht.
3. De host stuurt een DHCPv6 SOLICIT-bericht.
4. De DHCPv6-server antwoordt met een ADVERTISE-bericht.
5. De host antwoordt aan de DHCPv6 server.
6. De DHCPv6-server stuurt een REPLY-bericht.

*Stap 1: De host stuurt een RS bericht naar alle IPv6 ingeschakelde routers*

image::{42}8.3.1.a.png[]

*Stap 2: De router antwoordt met een RA bericht*
R1 ontvangt het RS en antwoordt met een RA dat aangeeft dat de client communicatie met een DHCPv6-server moet starten.

image::{42}8.3.1.b.png[]

*Stap 3: De host stuurt nu een router SOLICIT bericht*

De client, nu een DHCPv6-client, moet een DHCPv6-server vinden en stuurt een DHCPv6 SOLICIT-bericht naar het gereserveerde IPv6 multicast all-DHCPv6-servers-adres ff02::1:2. 
Dit multicast adres heeft link-local scope, wat betekent dat routers de berichten niet doorsturen naar andere netwerken.

image::{42}8.3.1c.png[]

*Stap 4: De DHCPv6 server antwoordt met een ADVERTISE bericht*

Een of meer DHCPv6-servers antwoorden met een DHCPv6 ADVERTISE unicast bericht. 
Het ADVERTISE-bericht informeert de DHCPv6-client dat de server beschikbaar is voor DHCPv6-dienst.

image::{42}8.3.1d.png[]

*Stap 5: De host geeft antwoord aan de DHCPv6 server*

Het antwoord van PC1 hangt af van het gebruik van stateful of stateless DHCPv6:

1. Stateless DHCPv6 client - De client maakt een IPv6-adres aan met behulp van de prefix in het RA-bericht en een zelfgegenereerde Interface ID. De client stuurt vervolgens een DHCPv6 INFORMATION-REQUEST-bericht naar de DHCPv6-server waarin om aanvullende configuratieparameters wordt gevraagd (bijv. DNS-serveradres).
2. Stateful DHCPv6-client - De client stuurt een DHCPv6 REQUEST-bericht naar de DHCPv6-server om alle benodigde IPv6-configuratieparameters te verkrijgen.


image::{42}8.3.1e.png[]

*Stap 6: De DHCPv6 server geeft een REPLY bericht*

De server stuurt een DHCPv6 REPLY unicast bericht naar de client. De inhoud van het bericht varieert naargelang het een antwoord is op een REQUEST- of een INFORMATION-REQUEST-bericht.

NOTE: De client zal het bron-IPv6 Link-local adres van de RA gebruiken als zijn standaard gateway-adres. Een DHCPv6-server verschaft deze informatie niet.


image::{42}8.3.1f.png[]

==== 8.3.2 De werking van STATELESS DHCPv6 

De stateless DHCPv6-server verschaft alleen informatie die identiek is voor alle apparaten op het netwerk, zoals het IPv6-adres van een DNS-server.

Dit proces staat bekend als stateless DHCPv6 omdat de server geen client-statusinformatie bijhoudt (d.w.z. een lijst van beschikbare en toegewezen IPv6-adressen). 
De stateless DHCPv6-server verschaft alleen configuratieparameters voor cliënten, geen IPv6-adressen.

De afbeelding illustreert de werking van stateless DHCPv6.

image::{42}8.3.2a.png[]

1. PC1 ontvangt een stateless DHCP RA bericht. Dit RA bericht bevat de *network prefix* en de *prefix lengte*
De *M* vlag bedoeld voor statefull DHCP staat op de standaardwaarde _0_. De A=1 vlag vertelt de client SLAAC te gebruiken. De O=1 vlag vertelt de client, dat het aanvullende informatie van de stateless DHCPv6 server moet ophalen.

2. De client verstuurt een DHCPv6 SOLICIT bericht, die op zoek gaat naar de stateless DHCPv6 server voor de aanvullende informatie. (Bijv. DNS server adres).


==== 8.3.3 Stateless DHCP inschakelen op een interface

Stateless DHCPv6 wordt ingeschakeld op een routerinterface met de `ipv6 nd other-config-flag` in de interfaceconfiguratie modus. 
Dit zet de O-vlag op 1.

De gemarkeerde uitvoer bevestigt dat de RA de ontvangende hosts zal vertellen stateless autoconfigure te gebruiken (A flag = 1) en contact op te nemen met een DHCPv6-server om andere configuratie-informatie te verkrijgen (O flag = 1).

image::{42}8.3.3a.png[]

NOTE: Je kunt de `no ipv6 nd other-config-flag` gebruiken om de interface terug te zetten naar de standaard SLAAC only optie (O flag = 0).

==== 8.3.4 De werking van statefull DHCPv6

Deze optie lijkt het meest op DHCPv4. 
In dit geval vertelt het RA-bericht de client om alle adresseringsinformatie te halen van een stateful DHCPv6-server, behalve het standaard gateway-adres dat het bron-IPv6 link-local adres van de RA is.

Dit staat bekend als stateful DHCPv6 omdat de DHCPv6-server IPv6-statusinformatie bijhoudt. 
Dit is vergelijkbaar met een DHCPv4 server die adressen voor IPv4 toekent.

De afbeelding illustreert de stateful DHCPv6 werking.

image::{42}8.3.4a.png[]

1. PC1 ontvangt een DHCPv6 RA bericht waarin de _O_ vlag op *0* staat en de _M_ vlag op *1*. Dit betekent dat de host alle IPv6 informatie van een stateful DHCPv6 zal ontvangen.
2. PC1 verstuurt een DHCPv6 SOLICIT bericht om de stateful DHCPv6 server op te zoeken.

NOTE: Als A=1 en M=1, zullen sommige besturingssystemen zoals Windows een IPv6 adres aanmaken met SLAAC en een ander adres ophalen van de stateful DHCPv6 server. 
In de meeste gevallen is het aan te bevelen de A vlag handmatig op 0 te zetten.

==== 8.3.5 Stateful DHCPv6 op een interface inschakelen

Stateful DHCPv6 wordt ingeschakeld op een routerinterface met de `ipv6 nd managed-config-flag` in de interface-configuratie modus. 
Dit zet de M vlag op 1. 
De `ipv6 nd prefix default no-autoconfig` interface opdracht, schakelt SLAAC uit door de A vlag op 0 te zetten.

De gemarkeerde uitvoer in het voorbeeld bevestigt dat de RA de host zal vertellen om alle IPv6 configuratie-informatie van een DHCPv6 server te verkrijgen (M flag = 1).

image::{42}8.3.5a.png[]


=== 8.4 Een DHCPv6 server configureren

==== 8.4.1 DHCPv6 Router rollen

Cisco IOS-routers zijn krachtige apparaten. 
In kleinere netwerken is het niet nodig om afzonderlijke apparaten te hebben, die beschikken over een  DHCPv6-server, -client of -relayagent. 
Een Cisco IOS-router kan worden geconfigureerd om DHCPv6-serverservices te bieden.

Hij kan met name als een van de volgende worden geconfigureerd:

1. DHCPv6 Server - Router biedt stateless of stateful DHCPv6-diensten.
2. DHCPv6 Client - Routerinterface verwerft een IPv6 IP-configuratie van een DHCPv6-server.
3. DHCPv6 Relay Agent - Router levert DHCPv6-doorstuurservices wanneer de client en de server zich op verschillende netwerken bevinden.

==== 8.4.2 Een stateless DHCP server configureren

De stateless DHCPv6-serveroptie vereist dat de router de IPv6-netwerkadresseringsinformatie in RA-berichten adverteert. 
De cliënt moet echter contact opnemen met een DHCPv6-server voor meer informatie.

Bestudeer het voorbeeld hier onder om te leren hoe de stateless DHCPv6-servermethode geconfigureerd moet worden.
	
image::{42}8.4.2a.png[]


In dit voorbeeld zal R1 SLAAC-diensten leveren voor de host IPv6-configuratie en DHCPv6-diensten.

Er zijn vijf stappen om een router als een stateless DHCPv6 server in te stellen en te verifiëren:

*Stap 1. Schakel IPv6-routering in.*
Het commando `ipv6 unicast-routing` is vereist om IPv6-routering in te schakelen. 
Hoewel het niet nodig is dat de router een stateless DHCPv6 server is, is het wel nodig dat de router ICMPv6 RA berichten kan genereren.

image::{42}8.4.2b.png[]

*Stap 2. Definieer een DHCPv6 poolnaam.*

Creëer de DHCPv6 pool met het `ipv6 dhcp pool POOL-NAME` global config commando. 
Dit gaat naar de DHCPv6 pool sub-configuratie modus zoals geïdentificeerd door de `Router(config-dhcpv6)#` prompt.

NOTE: De poolnaam hoeft geen hoofdletter te zijn. 
Echter, het gebruik van een naam met hoofdletters maakt het makkelijker om hem in een configuratie te zien.	

image::{42}8.4.2c.png[]


*Stap 3. Configureer de DHCPv6 pool.*

R1 zal geconfigureerd worden om aanvullende  DHCP informatie te verstrekken, inclusief _DNS server_ adres en _domeinnaam_, zoals getoond in de commando uitvoer.

image::{42}8.4.2d.png[]


*Stap 4. Koppel de DHCPv6 pool aan een interface.*

De DHCPv6 pool moet aan de interface gekoppeld worden met het `ipv6 dhcp server POOL-NAME` commando in de interface config modus, zoals in de uitvoer getoond wordt.
De router antwoordt op stateless DHCPv6-verzoeken op deze interface met de informatie in de pool. 
De O flag moet handmatig veranderd worden van 0 naar 1 met het interface commando `ipv6 nd other-config-flag`. 
RA berichten verzonden op deze interface geven aan dat bijkomende informatie beschikbaar is van een stateless DHCPv6 server. 
De A vlag is standaard 1, wat cliënten vertelt om SLAAC te gebruiken om hun eigen GUA aan te maken.

image::{42}8.4.2e.png[]


*Stap 5. Verifieer dat de hosts IPv6 adresseringsinformatie hebben ontvangen.*


Om stateless DHCP op een Windows host te controleren, kun je de opdracht `ipconfig /all` gebruiken. 
Op Linux hosts kun je contrleren of de hosts IPv6-adresseringsinformatie hebben ontvangen met het commando `ip a`.

De uitvoer van het voorbeeld toont de instellingen op PC1.

NOTE: PC1 heeft zijn IPv6 GUA aangemaakt met de 2001:db8:acad:1::/64 prefix. 

NOTE: De standaard gateway is het IPv6 link-local adres van R1. Dit bevestigt dat PC1 zijn IPv6 configuratie heeft afgeleid van de RA van R1.

De gemarkeerde uitvoer bevestigt dat PC1 de domeinnaam en DNS-serveradresinformatie van de stateless DHCPv6-server heeft geleerd.

image::{42}8.4.2f.png[]

==== 8.4.3 Configureer een stateless DHCPv6 client

Een router kan ook een DHCPv6-client zijn en een IPv6-configuratie krijgen van een DHCPv6-server, zoals een router die als een DHCPv6-server functioneert. 
In de afbeelding is R1 een stateless DHCPv6 server.

image::{42}8.4.3a.png[]

De vijf stappen om een router als IPv6 DHCP client te configureren en te controleren:

*1. Schakel IPv6 routing in*
De DHCPv6 client router moet ipv6 unicast-routing ingeschakeld hebben

image::{42}8.4.3b.png[]

*2. Configureer de client-router om een LLA aan te maken*

De client-router moet een link-local adres hebben. 
Een IPv6 link-local adres wordt aangemaakt op een routerinterface wanneer een globaal unicast adres is geconfigureerd. 
Het kan ook zonder GUA worden aangemaakt met de opdracht `ipv6 enable` interface configuration. 
Cisco IOS gebruikt EUI-64 om een willekeurige interface-ID te maken.

In de uitvoer is de opdracht `ipv6 enable` geconfigureerd op de interface Gigabit Ethernet 0/0/1 van de client-router R3.

image::{42}8.4.3c.png[]

*3. Configureer de clientrouter SLAAC te gebruiken*

De client-router moet geconfigureerd zijn om SLAAC te gebruiken om een IPv6-configuratie te maken. 
Het commando `ipv6 address autoconfig` schakelt de automatische configuratie van IPv6-adressering met SLAAC in.

image::{42}8.4.3d.png[]

*4. Controleer of de client-router een GUA heeft gekregen*

Gebruik het `show ipv6 interface brief` commando om de host configuratie te verifiëren zoals getoond. 
De uitvoer bevestigt dat de interface G0/0/1 op R3 een geldige GUA toegewezen kreeg.

NOTE: het kan een paar seconden duren voordat de interface het proces heeft voltooid

image::{42}8.4.3e.png[]

*5. Controleer of de client-router aanvullende DHCPv6 informatie heeft gekregen*

Het commando `show ipv6 dhcp interface g0/0/1` bevestigt dat de DNS en domeinnamen ook door R3 werden geleerd.

image::{42}8.4.3f.png[]

==== 8.4.4 Configureer een stateless DHCPv6 server

De "stateful" DHCP-serveroptie vereist dat de IPv6-ingeschakelde router de host vertelt contact op te nemen met een DHCPv6-server om alle benodigde IPv6-netwerkadresseringsinformatie te op te halen.

In de figuur zal R1 stateful DHCPv6-diensten aan alle hosts op het lokale netwerk leveren. 
Het configureren van een "stateful" DHCPv6 server is vergelijkbaar met het configureren van een "stateless" server. 
Het belangrijkste verschil is dat een "stateful" DHCPv6 server ook IPv6-adresseringsinformatie bevat, vergelijkbaar met een DHCPv4 server.

image::{42}8.4.4a.png[]


*1. Schakel IPv6 routing in*

Het ipv6 unicast-routing commando is nodig om IPv6 routing in te schakelen.

image::{42}8.4.3b.png[]

*2. Definieer een DHCPv6 pool naam*

Maak de DHCPv6 pool aan met het `ipv6 dhcp pool POOL-NAME` global config commando.

image::{42}8.4.4b.png[]

*3. Configureer de DHCP pool*

R1 zal geconfigureerd worden om IPv6 adressering, DNS server adres, en domeinnaam te verstrekken aan clients, zoals getoond in de opdrachtuitvoer. 
Met stateful DHCPv6 moeten alle adressering en andere configuratieparameters worden toegewezen door de DHCPv6 server. 
Het `adres prefix` commando wordt gebruikt om de pool van adressen aan te geven die door de server worden toegewezen. 
Andere informatie die door de stateful DHCPv6 server wordt verschaft omvat gewoonlijk het DNS serveradres en de domeinnaam, zoals in de uitvoer wordt getoond.

WARNING: Dit voorbeeld stelt de DNS server in op Google's publieke DNS server.

image::{42}8.4.4c.png[]

*4. Koppel de DHCPv6 pool aan een interface*
Het voorbeeld toont de volledige configuratie van de GigabitEthernet 0/0/1 interface op R1.

De DHCPv6 pool moet aan de interface gebonden worden met het ipv6 dhcp server POOL-NAME interface config commando.

1. De M flag wordt manueel veranderd van 0 naar 1 met het interface commando `ipv6 nd managed-config-flag`.
2. De A flag wordt manueel veranderd van 1 naar 0 met het interface commando `ipv6 nd prefix default no-autoconfig`. De A vlag kan op 1 gelaten worden, maar sommige cliëntbesturingssystemen zoals Windows zullen een GUA aanmaken met SLAAC en een GUA krijgen van de stateful DHCPv6 server. 
De A vlag op 0 zetten vertelt de client om SLAAC niet te gebruiken om een GUA aan te maken.
3. Het `ipv6 dhcp server` commando bindt de DHCPv6 pool aan de interface. 
R1 zal nu antwoorden met de informatie vervat in de pool wanneer het stateful DHCPv6 verzoeken ontvangt op deze interface.

image::{42}8.4.4d.png[]

NOTE: Je kan het no ipv6 nd managed-config-flag commando gebruiken om de M flag terug op zijn standaard van 0 te zetten. Het no ipv6 nd prefix default no-autoconfig commando zet de A flag terug op zijn standaard van 1.

*5. Controleer of hosts de gevraagde ipv6 informatie hebben gekregen*
Voor controle op een Windows host, kun je het commando `ipconfig /all` gebruiken. Hiermee controleer je de stateless DHCP configuratiemethode. 
Controleer of Linux hosts IPv6-adresseringsinformatie hebben ontvangen met het commando `ip a`.

De uitvoer toont de instellingen op PC1. De gemarkeerde uitvoer laat zien dat PC1 zijn IPv6 GUA van een stateful DHCPv6 server heeft ontvangen.

image::{42}8.4.4f.png[]

==== 8.4.5 Configureer een stateful DHCPv6 client

Een router kan ook een DHCPv6-client zijn. 
De client-router dient `ipv6 unicast-routing` ingeschakeld te hebben en een IPv6 link-local adres om IPv6-berichten te kunnen verzenden en ontvangen.

Raadpleeg de voorbeeldtopologie om te leren hoe de stateful DHCPv6-client geconfigureerd moet worden.

image::{42}8.4.5a.png[]

Er zijn vijf stappen om een router als een stateful DHCPv6 server te configureren en te verifiëren.

*Stap 1. Schakel IPv6-routering in.*

Het ipv6 unicast-routing commando is nodig om IPv6 routing in te schakelen.

image::{42}8.4.3b.png[]

*Stap 2. Configureer de client router om een LLA aan te maken.*

In de uitvoer is het `ipv6 enable` commando geconfigureerd op de R3 Gigabit Ethernet 0/0/1 interface. 
Dit stelt de router in staat om een IPv6 LLA aan te maken zonder een GUA nodig te hebben.

image::{42}8.4.5b.png[]

*Stap 3. Configureer de client router om DHCPv6 te gebruiken.*

Het `ipv6 address dhcp` commando configureert R3 om zijn IPv6 adresseringsinformatie van een DHCPv6 server te vragen.

image::{42}8.4.5c.png[]

*Stap 4. Controleer of aan de client-router een GUA is toegewezen.*

Gebruik het commando `show ipv6 interface brief` om de hostconfiguratie te controleren zoals afgebeeld.

image::{42}8.4.5d.png[]

*Stap 5. Controleer of de client-router andere noodzakelijke DHCPv6-informatie heeft ontvangen.*

Het commando `show ipv6 dhcp interface g0/0/1` bevestigt dat de DNS en domeinnamen door R3 werden geleerd.

image::{42}8.4.5e.png[]

==== 8.4.6 Commando's om DHCPv6 te controleren


*show ipv6 dhcp pool*

Gebruik de commando's `show ipv6 dhcp pool` en `show ipv6 dhcp binding` om de werking van DHCPv6 op een router te controleren.

Het commando `show ipv6 dhcp pool` controleert de naam van de DHCPv6 pool en zijn parameters. 
Het commando identificeert ook het aantal actieve clients. 
In dit voorbeeld heeft de IPV6-STATEFUL pool momenteel 2 clients, wat aangeeft dat PC1 en R3 hun IPv6 globaal unicast adres van deze server ontvangen.

Wanneer een router stateful DHCPv6-diensten levert, houdt hij ook een database bij van toegewezen IPv6-adressen.

image::{42}8.4.6a.png[]


*show ipv6 dhcp binding*

Gebruik de uitvoer van het commando `show ipv6 dhcp binding` om het IPv6 link-local adres van de client en het globale unicast adres dat door de server is toegewezen weer te geven.

De uitvoer toont de huidige stateful binding op R1. De eerste client in de uitvoer is PC1 en de tweede client is R3.

Deze informatie wordt bijgehouden door een stateful DHCPv6-server. Een stateless DHCPv6-server zou deze informatie niet bijhouden.

image::{42}8.4.6b.png[]


==== 8.4.7 Configureer een dhcpv6 relay agent

Als de DHCPv6-server zich op een ander netwerk dan de cliënt bevindt, kan de IPv6-router als een DHCPv6-relayagent worden geconfigureerd. 
De configuratie van een DHCPv6-relayagent is vergelijkbaar met de configuratie van een IPv4-router als een DHCPv4-relay.

In de figuur is R3 geconfigureerd als een stateful DHCPv6-server. 
PC1 bevindt zich op het 2001:db8:acad:2::/64-netwerk en heeft de diensten van een stateful DHCPv6-server nodig om zijn IPv6-configuratie te verkrijgen. 
R1 moet worden geconfigureerd als de DHCPv6 Relay Agent.
	
image::{42}8.4.7a.png[]

De opdrachtsyntaxis om een router als een DHCPv6 relay agent te configureren is als volgt:

image::{42}8.4.7b.png[]

Dit commando wordt geconfigureerd op de interface tegenover de DHCPv6 clients en specificeert het DHCPv6 server adres en de egress interface om de server te bereiken, zoals getoond in de uitvoer. De egress interface is alleen nodig als het next-hop adres een LLA is.

image::{42}8.4.7c.png[]

==== 8.4.8 Controleer de DHCPv6 relay agent

Controleer of de DHCPv6 relay agent operationeel is met de commando's `show ipv6 dhcp interface` en `show ipv6 dhcp binding`. 
Controleer of Linux hosts IPv6-adresseringsinformatie hebben ontvangen met het commando `ip a`.
Controleer of Windows hosts IPv6-adresseringsinformatie hebben ontvangen met het commando `ipconfig /all`.

*show ipv6 dhcp interface*

De DHCPv6 relay agent kan geverifieerd worden met het commando `show ipv6 dhcp interface`. Dit zal verifiëren dat de interface G0/0/1 in relay-modus is.

image::{42}8.4.8a.png[]

*show ipv6 dhcp binding*

Gebruik op R3 het commando `show ipv6 dhcp binding` om te controleren of er hosts zijn waaraan een IPv6-configuratie is toegewezen.

Merk op dat een client link-local adres een IPv6 GUA toegewezen heeft gekregen. We kunnen aannemen dat dit PC1 is.	

image::{42}8.4.8b.png[]

*Linux: `ip a` Windows: `ipconfig /all`*

Gebruik tenslotte `ip a` (Linux) of `ipconfig /all` (Windows)  op PC1 om te bevestigen dat het een IPv6-configuratie toegewezen heeft gekregen. 
Zoals je kunt zien, heeft PC1 inderdaad zijn IPv6 configuratie van de DHCPv6 server ontvangen.

image::{42}8.4.8c.png[]

=== 8.5 LABS activiteiten en toets

==== 8.5.1 LAB Configureer DHCPv6

In dit practicum zul je de volgende doelstellingen bereiken:

- Deel 1: Bouw het netwerk op en configureer basisinstellingen voor apparaten
- Deel 2: Verifieer SLAAC adrestoewijzing van R1
- Deel 3: Configureer en verifieer een Stateless DHCPv6 Server op R1
- Deel 4: Configureer en verifieer een Stateful DHCPv6 Server op R1
- Deel 5: Een DHCPv6-relay op R2 configureren en verifiëren

{42}8.5.1-lab---configure-dhcpv6.pdf[LAB - Configureer DHCPv6]


==== 8.5.2 Wat heb je in deze module geleerd?

*IPv6 GUA-toewijzing*

Op een router wordt een IPv6 globaal unicast adres (GUA) handmatig geconfigureerd met de opdracht ipv6-adres ipv6-adres/prefix-lengte interfaceconfiguratie. Wanneer automatische IPv6-adressering geselecteerd is, zal de host proberen automatisch IPv6-adresinformatie op de interface te verkrijgen en te configureren. Het IPv6 link-local adres wordt automatisch aangemaakt door de host wanneer deze opstart en de Ethernet interface actief is. Standaard adverteert een IPv6-geschikte router zijn IPv6-informatie zodat een host dynamisch zijn IPv6-configuratie kan aanmaken of verkrijgen. De IPv6 GUA kan dynamisch worden toegewezen met behulp van stateless en stateful services. De beslissing hoe een client een IPv6 GUA zal verkrijgen hangt af van de instellingen in het RA bericht. Een ICMPv6 RA bericht bevat drie vlaggen om de dynamische opties te identificeren die beschikbaar zijn voor een host:

- *A flag* - Dit is de Address Autoconfiguration flag. Gebruik SLAAC om een IPv6 GUA aan te maken.
- *O flag* - Dit is de Other Configuration flag. Verkrijg Overige informatie van een stateless DHCPv6 server.
- *M vlag* - Dit is de Managed Address Configuration vlag. Gebruik een stateful DHCPv6 server om een IPv6 GUA te verkrijgen.

*SLAAC*

De SLAAC-methode stelt hosts in staat om hun eigen unieke IPv6 global unicast-adres te creëren zonder de diensten van een DHCPv6-server. 
SLAAC, dat stateless is, gebruikt ICMPv6 RA-berichten om adresserings- en andere configuratie-informatie uit te delen, die normaal door een DHCP-server zou worden verstrekt. 
SLAAC kan worden geïmplementeerd als alleen SLAAC, of SLAAC met DHCPv6. 
Om het verzenden van RA-berichten mogelijk te maken, moet een router lid worden van de _IPv6 all-routers_ groep met het `ipv6 unicast-routing` global config commando. 
Gebruik het commando `show ipv6 interface` om te controleren of een router is ingeschakeld. 
De SLAAC only methode is standaard ingeschakeld wanneer het `ipv6 unicast-routing` commando is geconfigureerd. 
Alle ingeschakelde Ethernet interfaces met een geconfigureerde IPv6 GUA zullen RA berichten beginnen te versturen met de A vlag op 1, en de O en M vlaggen op 0. 
De A = 1 vlag informeert de client om zijn eigen IPv6 GUA te creëren met de prefix geadverteerd in de RA. 
De O =0 en M=0 vlaggen instrueren de client om uitsluitend de informatie in het RA bericht te gebruiken. 
Een router verstuurt elke 200 seconden RA berichten. 
Hij zal echter ook een RA-bericht verzenden als hij een RS-bericht van een host ontvangt. 
Met SLAAC verkrijgt een host gewoonlijk zijn 64-bit IPv6-subnetinformatie uit het RA-bericht van de router. 
Hij moet echter de resterende 64-bits interface-identifier (ID) genereren met behulp van een van de volgende twee methoden: 

1. willekeurig gegenereerd
2. EUI-64. 

Het DAD-proces wordt door een host gebruikt om te verzekeren dat de IPv6 GUA uniek is. 
DAD wordt geïmplementeerd met behulp van ICMPv6. 
Om DAD uit te voeren stuurt de host een ICMPv6 NS bericht met een speciaal geconstrueerd multicast adres, dat een solicited-node multicast adres wordt genoemd. 
Dit adres dupliceert de laatste 24 bits van het IPv6-adres van de host.


*DHCPv6*

De host begint de DHCPv6-client/servercommunicatie nadat stateless DHCPv6 of stateful DHCPv6 is aangegeven in de RA. 
Server-naar-client DHCPv6-berichten gebruiken UDP-bestemmingspoort 546, terwijl client-naar-server DHCPv6-berichten UDP-bestemmingspoort 547 gebruiken. 
De stateless DHCPv6-optie informeert de client om de informatie in het RA-bericht te gebruiken voor adressering, maar aanvullende configuratieparameters zijn beschikbaar van een DHCPv6-server. 
Dit wordt stateless DHCPv6 genoemd omdat de server geen statusinformatie over de cliënt bijhoudt. 
Stateless DHCPv6 wordt ingeschakeld op een routerinterface met het commando `ipv6 nd other-config-flag` in de interfaceconfiguratie. 
Dit zet de O-vlag op 1. 
In stateful DHCPv6 vertelt het RA-bericht de cliënt om alle adresseringsinformatie te verkrijgen van een stateful DHCPv6-server, behalve het standaard gateway-adres dat het bron-IPv6 link-local adres van de RA is. 
Het wordt "stateful" genoemd omdat de DHCPv6-server IPv6-statusinformatie bijhoudt. 
Stateful DHCPv6 wordt ingeschakeld op een routerinterface met het commando `ipv6 nd managed-config-flag` in de interfaceconfiguratie. 
Hiermee wordt de M-vlag op 1 gezet.

*DHCPv6-server configureren*

Een Cisco IOS-router kan worden geconfigureerd om DHCPv6-serverservices te leveren als een van de volgende drie typen: 

1. DHCPv6-server
2. DHCPv6-client
3. DHCPv6-relayagent

De stateless DHCPv6-serveroptie vereist dat de router de IPv6-netwerkadresseringsinformatie in RA-berichten adverteert. 
Een router kan ook een DHCPv6-client zijn en een IPv6-configuratie van een DHCPv6-server krijgen. 
De stateful DHCP-serveroptie vereist dat de IPv6-geschikte router de host vertelt contact op te nemen met een DHCPv6-server om alle benodigde IPv6-netwerkadresseringsinformatie te verkrijgen. 
Om een client-router een DHCPv6-router te laten zijn, dient deze ipv6 unicast-routing ingeschakeld te hebben en een IPv6-link-local-adres om IPv6-berichten te kunnen verzenden en ontvangen. 
Gebruik de commando's `show ipv6 dhcp pool` en `show ipv6 dhcp binding` om de werking van DHCPv6 op een router te controleren. 
Indien de DHCPv6 server zich op een ander netwerk bevindt dan de client, dan kan de IPv6 router als een DHCPv6 relay agent geconfigureerd worden met het `ipv6 dhcp relay destination ipv6-address [interface-type interface-number]` commando. 
Dit commando wordt geconfigureerd op de interface tegenover de DHCPv6 clients en specificeert het DHCPv6 server adres en de egress interface om de server te bereiken. 
De egress interface is alleen nodig als het next-hop adres een LLA is. 
Controleer of de DHCPv6 relay agent operationeel is met de commando's `show ipv6 dhcp interface` en `show ipv6 dhcp binding`.
	
==== 8.4.9 Toets (Uitvoeren in Netacad)

== {42}[N42 CCNA2 Switching en Routing Essentials (SWRE)]
