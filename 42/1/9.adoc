:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud
:42: https://kbase.nl/datacom/42/1/
:soc: https://b.socrative.com/login/student/

== {42}[N42 CCNA2 Switching en Routing Essentials (SWRE)]


== 9. FHRP concepten

=== 9.0 Introductie

==== 9.0.1 Wat leer ik in deze module?

Welkom bij FHRP Concepts!

Je netwerk is 'up and running'. 
Je hebt Layer 2 redundantie overwonnen zonder enige Layer 2 loops. 
Al je apparaten krijgen hun adressen dynamisch. 
Je bent goed in netwerkbeheer! 
Maar, wacht. Een van je routers, de default gateway router in feite, is uitgevallen. 
Geen van je hosts kan berichten versturen buiten het directe netwerk. 
Het gaat een tijdje duren om deze standaard gateway router weer werkend te krijgen. 
Je hebt een hoop boze mensen die je vragen hoe snel het netwerk weer 'up' zal zijn.

Je kunt dit probleem eenvoudig voorkomen. 
*First Hop Redundancy Protocols (FHRP's)* zijn de oplossing die je nodig hebt. 
Deze module bespreekt wat FHRP doet, en alle types FHRP's die er beschikbaar zijn. 
Een van deze typen is een Cisco-eigen FHRP genaamd *Hot Standby Router Protocol (HSRP)*. 
Je leert hoe HSRP werkt en voltooit vervolgens een Packet Tracer activiteit waarin je HSRP configureert en verifieert. 

==== 9.0.2 Wat ga leren?

[%header, cols=2]
|===
|Onderwerp|Doel
|First Hop Redundancy Protocols| Leg het doel en de werking uit van protocollen voor eerste-sprong-redundantie.
|HSRP|Uitleggen hoe HSRP werkt
|===

=== 9.1 First Hop Redundancy Protocols

==== 9.1.1 Beperkingen van de default gateway

Als een router of routerinterface (die als standaard gateway dient) uitvalt, zijn de hosts die met die standaard gateway geconfigureerd zijn geïsoleerd van netwerken buiten het netwerk. 
Er is een mechanisme nodig om alternatieve standaard gateways te verschaffen in geswitchde netwerken waar twee of meer routers met dezelfde VLAN's zijn verbonden. 
Dat mechanisme wordt geleverd door *first hop redundantieprotocollen (FHRP's)*.

In een switched netwerk ontvangt elke cliënt slechts één standaard gateway. 
Er is geen manier om een secundaire gateway te gebruiken, zelfs als er een tweede pad bestaat om pakketten van het lokale segment af te voeren.

In de figuur is R1 verantwoordelijk voor het routeren van pakketten van PC1. 
Als R1 uitvalt,  kunnen de routeringsprotocollen dynamisch convergeren (samengaan).
R2 routeert nu pakketten van externe netwerken die door R1 zouden zijn gegaan. 
Verkeer van het interne netwerk dat met R1 is verbonden, waaronder verkeer van werkstations, servers en printers die met R1 als standaard gateway zijn geconfigureerd, worden echter nog steeds naar R1 gestuurd en gedropt.

NOTE: In het kader van de bespreking van routerredundantie is er geen functioneel verschil tussen een Layer 3 switch en een router op de distributielaag. 
In de praktijk is het gebruikelijk dat een Layer 3 switch optreedt als de standaard gateway voor elk VLAN in een geswitched netwerk. 
Deze bespreking richt zich op de functionaliteit van routing, ongeacht het gebruikte fysieke apparaat.

image::{42}911a.png[title=PC1 kan de default gateway niet bereiken]


Eindapparaten worden gewoonlijk geconfigureerd met een enkel IPv4-adres voor een default gateway. 
Dit adres verandert niet wanneer de netwerktopologie verandert. 
Als dat standaard IPv4-gatewayadres niet kan worden bereikt, kan het lokale apparaat geen pakketten van het lokale netwerksegment verzenden, waardoor het effectief wordt losgekoppeld van andere netwerken. 
Zelfs als er een redundante router bestaat, die als standaardgateway voor dat segment zou kunnen dienen, is er geen dynamische methode waarmee deze apparaten het adres van een nieuwe standaardgateway kunnen bepalen.

NOTE: IPv6-apparaten ontvangen hun standaard gateway-adres dynamisch van de ICMPv6 Router Advertisement. 
IPv6-apparaten hebben echter baat bij een snellere failover naar de nieuwe standaard gateway wanneer FHRP wordt gebruikt.

==== 9.1.2 Router Redundancy

Eén manier om een single point of failure (SPOF) bij de default gateway te voorkomen is het implementeren van een *virtuele router*. 
Om dit type routerredundantie te implementeren, worden meerdere routers geconfigureerd om samen te werken om de hosts op het LAN de illusie van één enkele router te geven, zoals in de figuur wordt getoond. 
Door een IP-adres en een MAC-adres te delen, kunnen twee of meer routers als een enkele virtuele router fungeren.

image::{42}912a.png[title=Met een virtuele router]

Het IPv4-adres van de virtuele router wordt geconfigureerd als de default gateway voor de werkstations op een bepaald IPv4-segment. 
Wanneer frames van hostapparaten naar de default gateway worden verzonden, gebruiken de hosts *ARP* om het MAC-adres vast te stellen, dat is gekoppeld aan het IPv4-adres van de standaardgateway. 
De ARP-omzetting levert het MAC-adres van de virtuele router op. 
Frames die naar het MAC-adres van de virtuele router worden gezonden, kunnen dan fysiek worden verwerkt door de op dat moment actieve router binnen de virtuele router groep. 
Een protocol wordt gebruikt om twee of meer routers aan te wijzen als de apparaten die verantwoordelijk zijn voor de verwerking van frames die naar het MAC- of IP-adres van een enkele virtuele router worden gezonden. 
Hostapparaten sturen verkeer naar het adres van de virtuele router. 
De fysieke router die dit verkeer doorstuurt is transparant voor de host-apparaten.

Een redundantieprotocol levert de techniek om te bepalen welke router de actieve rol bij het doorsturen van verkeer op zich moet nemen. 
Het bepaalt ook wanneer de doorsturende rol door een standby-router moet worden overgenomen. 
De overgang van de ene forwarding router naar de andere is transparant voor de eindapparaten.

Het vermogen van een netwerk om dynamisch te herstellen van het uitvallen van een apparaat dat als standaardgateway fungeert, staat bekend als *first-hop redundantie*.

==== 9.1.3 Stappen wanneer de actieve router uitvalt

Wanneer de actieve router uitvalt, schakelt het redundantieprotocol de standby-router over naar de nieuwe actieve routerrol, zoals in de figuur wordt getoond. 
Dit zijn de stappen die plaatsvinden wanneer de actieve router uitvalt:

1. De standby router stopt met het ontvangen van Hello berichten van de doorsturende router.
2. De standby-router neemt de rol van de doorsturende router op zich.
3. Omdat de nieuwe doorsturende router zowel het IPv4- als het MAC-adres van de virtuele router overneemt, ondervinden de hostapparaten geen onderbreking in de dienstverlening.

image::{42}913a.png[title=De standby router wordt de forwarding router]

==== 9.1.4 FHRP opties

Welke FHRP in een productieomgeving wordt gebruikt hangt grotendeels af van de apparatuur en de behoeften van het netwerk. In de tabel staan alle beschikbare opties voor FHRP's.

[%header, cols=2]
|===
|Type Protocol|Beschrijving
|Hot Standby Router Protocol (HSRP)|HRSP is een FHRP van Cisco dat is ontworpen om transparante failover van een first-hop IPv4-apparaat mogelijk te maken. HSRP biedt hoge netwerkbeschikbaarheid door first-hop routeringsredundantie te bieden voor IPv4-hosts op netwerken die zijn geconfigureerd met een IPv4-standaardgatewayadres. HSRP wordt gebruikt in een groep routers voor het selecteren van een actief apparaat en een stand-by apparaat. In een groep van apparaatinterfaces is het actieve apparaat het apparaat dat wordt gebruikt voor het routeren van pakketten; het standby-apparaat is het apparaat dat het overneemt wanneer het actieve apparaat uitvalt, of wanneer aan vooraf ingestelde voorwaarden wordt voldaan. De functie van de HSRP-standbyrouter is het bewaken van de operationele status van de HSRP-groep en het snel overnemen van de verantwoordelijkheid voor het doorsturen van pakketten als de actieve router uitvalt.
|HSRP voor IPv6|Dit is een FHRP van Cisco dat dezelfde functionaliteit biedt als HSRP, maar dan in een IPv6-omgeving. Een HSRP IPv6-groep heeft een virtueel MAC-adres dat is afgeleid van het HSRP-groepsnummer en een virtueel IPv6-link-lokaal adres dat is afgeleid van het virtuele MAC-adres van HSRP. Periodieke routeradvertenties (RA's) worden verzonden voor het HSRP virtuele IPv6 link-local adres wanneer de HSRP groep actief is. Wanneer de groep inactief wordt, stoppen deze RA's nadat een laatste RA is verzonden.
|Virtueel Router Redundantie Protocol versie 2 (VRRPv2)|Dit is een niet aan eigendomsrechten gebonden verkiezingsprotocol dat dynamisch de verantwoordelijkheid voor een of meer virtuele routers toewijst aan de VRRP-routers op een IPv4 LAN. Hierdoor kunnen meerdere routers op een multiaccess-link hetzelfde virtuele IPv4-adres gebruiken. Een VRRP-router wordt geconfigureerd om het VRRP-protocol uit te voeren in samenwerking met een of meer andere routers die aan een LAN zijn gekoppeld. In een VRRP-configuratie wordt één router gekozen als de virtuele router-master, waarbij de andere routers als backups fungeren, voor het geval de virtuele router-master uitvalt.
|VRRPv3|Dit verkiezingsprotocol biedt de mogelijkheid om IPv4- en IPv6-adressen te ondersteunen. VRRPv3 werkt in multi-vendor omgevingen en is schaalbaarder dan VRRPv2.
|GLBP (Gateway Load Balancing Protocol)|Dit is een FHRP van Cisco dat gegevensverkeer beschermt tegen een defecte router of circuit, net als HSRP en VRRP, terwijl het ook load balancing (ook wel het verdelen van belasting genoemd) tussen een groep redundante routers mogelijk maakt.
|GLBP voor IPv6|Dit is een FHRP van Cisco dat dezelfde functionaliteit biedt als GLBP, maar dan in een IPv6-omgeving. GLBP voor IPv6 biedt automatische router backup voor IPv6 hosts die geconfigureerd zijn met een enkele default gateway op een LAN. Meerdere first-hop routers op het LAN worden gecombineerd om een enkele virtuele first-hop IPv6 router te bieden terwijl de IPv6 pakket-forwarding load wordt gedeeld
|ICMP Router Discovery Protocol (IRDP)|IRDP, gespecificeerd in RFC 1256, is een legacy FHRP-oplossing. Met IRDP kunnen IPv4-hosts routers lokaliseren die IPv4-connectiviteit bieden met andere (niet-lokale) IP-netwerken.
|===

=== 9.2 HSRP

==== 9.2.1 Overzicht HSRP

